using Documenter
using DocumenterExample

# The makedocs call is exactly the same as in the GitHub / TravisCI case.
makedocs(
    modules = [DocumenterExample],
    sitename = "DocumenterExample",
    pages = [
        "Home" => "index.md",
    ],
    # The only (optional) non-standard thing is to specify the repo argument -- this will
    # will the "Edit on XYZ" links in the top right corner.
    repo = "https://bitbucket.org/mortenpi/documenterexample.jl/src/{commit}{path}#lines-{line}",
)

# For deploydocs, you need to set up the following TRAVIS_* environment variables.
withenv(
    # TRAVIS_REPO_SLUG is the only one that is repository-specific. This is because we are
    # actually deploying the HTML pages into a different repository.
    "TRAVIS_REPO_SLUG" => "bitbucket.org/mortenpi/mortenpi.bitbucket.io",
    "TRAVIS_PULL_REQUEST" => get(ENV, "BITBUCKET_BRANCH", nothing),
    "TRAVIS_BRANCH" => get(ENV, "BITBUCKET_BRANCH", nothing),
    "TRAVIS_TAG" => get(ENV, "BITBUCKET_TAG", nothing),
    "TRAVIS_PULL_REQUEST" => ("BITBUCKET_PR_ID" in keys(ENV)) ? "true" : "false",
) do
    deploydocs(
        # repo here needs to point to the BitBucket Pages repository
        repo = "bitbucket.org/mortenpi/mortenpi.bitbucket.io.git",
        # BitBucket pages are served from the master branch
        branch = "master",
        # As BitBucket Pages are shared between all the repositories of a user or a team,
        # it is best to deploy the docs to a subdirectory named after the package
        dirname = "DocumenterExample",
    )
end
