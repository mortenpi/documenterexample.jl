# Documenter example on BitBucket

The deployed documentation is available here: https://mortenpi.bitbucket.io/DocumenterExample/dev/

* Create `username.bitbucket.io` repository to host your [BitBucket pages][bb-pages-docs]

* Use [DocumenterTools][documentertools]' `Travis.genkeys()` to generate a SSH key pair
  and then:
  - Put the _public key_ as an SSH key to your personal or team user on BitBucket.
    Unfortunately you can not have per-repository write keys with BitBucket.
  - Put the _private key_ as a environment variable called `DOCUMENTER_KEY` to you package
    repository (under "Repository variable"; make sure you "Mask and encrypt" with the
    padlock icon).

    **Note:** the private key will give full write access to _all_ your repositories -- guard
    it well. Make sure it never gets exposed in your Pipeline builds.

* For this to work, you currently need the `master` version of Documenter. For that, run
  ```
  julia --project=docs/ -e 'using Pkg; pkg"add Documenter#master"'
  ```
  and then commit `docs/Manifest.toml` to your repository.

* See how the `bitbucket-pipelines.yml` and `make.jl` files are set up.


[documentertools]: https://github.com/JuliaDocs/DocumenterTools.jl/
[bb-pages-docs]: https://confluence.atlassian.com/bitbucket/publishing-a-website-on-bitbucket-cloud-221449776.html
