module DocumenterExample

"""
    greet()

This function prints "Hello World".
"""
greet() = print("Hello World!")

export greet

end # module
